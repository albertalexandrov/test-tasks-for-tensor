from bs4 import BeautifulSoup


class Cleaner:

    __slots__ = ()

    tags_to_remove = ["meta", "head", "script", "link", "noscript", "svg", "aside", "table"]

    def clean_empty_paragraphs(self, paragraphs: list) -> list:
        """ Метод, который очищает входной список абзацев от пустых абзацев

        :param list paragraphs: список абзацев, который необходимо очистить от пустый абзацев
        :return list: очищенный от пустых абзацев список
        """

        return [paragraph for paragraph in paragraphs if paragraph]

    def clean_tags(self, bs: BeautifulSoup) -> BeautifulSoup:
        """ Метод, который очищает входной объект BeautifulSoup от тегов из списка tags_to_remove

        :param BeautifulSoup bs: объект, который необходимо очистить
        :return BeautifulSoup: очищенный объект
        """

        for tag_to_remove in Cleaner.tags_to_remove:
            for tag in bs.find_all(tag_to_remove):
                tag.decompose()

        return bs
