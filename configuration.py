import yaml


class Configuration:

    def __init__(self, path: str = None):
        self._path = path if path else "config/config.yaml"
        self.config = yaml.load(open(self._path))
