import sys
from article_parser import ArticleParser

try:
    url = sys.argv[1]
    article = ArticleParser(url)
    article.minify()
except IndexError:
    print("Введите URL!")
