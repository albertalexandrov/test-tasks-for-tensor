from article_parser import ArticleParser

URLS = ["https://www.rbc.ru/politics/24/07/2018/5b57140e9a794793092acd1f",
        "https://lenta.ru/news/2018/07/23/why_not/",
        "http://www.ntv.ru/novosti/2050867/"]

for url in URLS:
    article = ArticleParser(url)
    article.minify()
