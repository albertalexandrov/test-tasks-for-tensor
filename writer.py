from configuration import Configuration


class Writer:

    __slots__ = "_saveto_path"

    config = Configuration().config

    def __init__(self):
        self._saveto_path = Writer.config.get("saveto_path")

    def write(self, url_tail: str, title: str, paragraphs: str) -> None:
        """ Метод, записывающий в файл заголовок и текст статьи

        :param str url_tail: хвост URL статьи
        :param str title: заголовок статьи
        :param str paragraphs: текст статьи
        :return: None
        """

        with open(f"{self._saveto_path}/{url_tail}.txt", "w", encoding="utf8") as file:
            file.write(title)
            file.write(paragraphs)
