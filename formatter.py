from configuration import Configuration


class Formatter:

    __slots__ = "_string_width"

    config = Configuration().config

    def __init__(self):
        self._string_width = Formatter.config.get("string_width")

    def format_paragraphs(self, paragraphs: list) -> str:
        """ Метод, форматирующий список абзацев в строку paragraph, ширина которой не превышает
        заданного количества символов.  Абзацы при этом отделены друг от друга одной пустой строкой

        :param list paragraphs: список абзацев
        :return str: отформатированные в заданную ширину абзацы
        """

        paragraph = ""
        for raw_paragraph in paragraphs:
            paragraph += self._format(raw_paragraph)

        return paragraph

    def format_title(self, title: str) -> str:
        """ Метод, форматирующий заголовок в ширину не более заданного количества символов и
        переводящий его в верхний регистр

        :param str title: заголовок
        :return str: отформатированный в заданную ширину заголовок
        """

        return self._format(title).upper()

    def _format(self, paragraph: str) -> str:
        """ Метод, который принимает абзац и возвращает отформатированный в заданную ширину абзац

        Сперва происходит разбивка абзаца на отдельные слова.  Потом - итерация по полученному списку слов
        Новое слово добавляется в строку line в том случае, если длина текущего слова плюс текущая длина line
        меньше заданной ширины строки (например, 80 символов).  В противном случае line копируется в
        переменную formatted_paragraph и обнуляется

        :param str paragraph: один абзац
        :return str: отформатированный в заданную ширину абзац
        """

        formatted_paragraph = line = ""
        words = paragraph.split()

        for index, word in enumerate(words):
            if len(line) + 1 + len(word) > self._string_width:  # 1 - пробел
                formatted_paragraph += line.rstrip() + "\n"
                line = ""
            line += f"{word} "
        formatted_paragraph += f"{line}\n\n"

        return formatted_paragraph
