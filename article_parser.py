from urllib.parse import urlparse
import os
import re

from bs4 import BeautifulSoup
import requests

from cleaner import Cleaner
from formatter import Formatter
from writer import Writer


class ArticleParser:

    __slots__ = "_domain", "_html", "_http_response", "_soup", "_url", "_url_tail"

    article_paragraph_regex = re.compile("[А-Я].+?[.!?]")

    cleaner = Cleaner()
    formatter = Formatter()
    writer = Writer()

    def __init__(self, url: str):
        self._domain = None
        self._html = None
        self._http_response = None
        self._soup = None
        self._url = url
        self._url_tail = None

    def minify(self):
        """ Основной метод класса, который по заданному URL извлекает страницу, обрабатывает ее и
        формирует текстовый файл с текстом статьи, представленной на данной странице
        """

        if self._url_is_available:
            title = self._get_title()
            title = ArticleParser.formatter.format_title(title)

            paragraphs = self._get_paragraphs()
            paragraphs = ArticleParser.formatter.format_paragraphs(paragraphs)

            ArticleParser.writer.write(self._url_tail, title, paragraphs)
        else:
            print("Ошибка!")

            # Обработка распространенных ошибок клиента

            if self._http_response.status_code == 400:
                print("Код ошибки 400. Неверный запрос.")
            elif self._http_response.status_code == 401:
                print("Код ошибки 401. Необходима авторизация.")
            elif self._http_response.status_code == 403:
                print("Код ошибка 403. Доступ запрещен.")
            elif self._http_response.status_code == 404:
                print("Код ошибки 404. Статьи по данному URL не найдено.")

            # Обработка распространенных ошибок сервера

            elif self._http_response.status_code == 500:
                print("Код ошибки 500. Внутренняя ошибка удаленного сервера.")
            elif self._http_response.status_code == 502:
                print("Код ошибки 502. Ошибочный шлюз.")
            elif self._http_response.status_code == 503:
                print("Код ошибки 503. Удаленный сервер недоступен.")

            # Для всех остальных случаев

            else:
                print(f"Код ошибки {self._http_response.status_code}")

    def _get_domain_tail(self) -> tuple:
        """ Метод, который возвращает домен сайта и оставшуюся часть пути (хвост)

        :return tuple: домен, например, https://lenta.ru и хвост
        """

        parse_result = urlparse(self._url)

        url_tail = parse_result.path.strip("/")
        url_tail = os.path.splitext(url_tail)[0]
        url_tail = url_tail.replace("/", "-")

        return f"{parse_result.scheme}://{parse_result.netloc}", url_tail

    def _get_paragraphs(self) -> list:
        """ Метод, возвращающий текст статьи в виде списка абзацев

        :return list: список абзацев
        """

        paragraphs = self._get_raw_paragraphs()
        parents = self._get_raw_parents(paragraphs)

        # Из всех родителей выбирается тот, который является общим для двух абзацев,
        # идущих друг за другом
        parent = None
        for index, parent in enumerate(parents):
            if parents[index] == parents[index + 1]:
                break

        parent = ArticleParser.cleaner.clean_tags(parent)
        parent = self._handle_a(parent)

        # Часто, но не всегда, абзацы будут обрамлены тегами р, что сильно упрощает анализ
        # Однако, в противном случае происходит поиск абзацев при помощи регулярных выражений
        p_tags = parent.find_all("p")
        if p_tags:
            paragraphs = [p_tag.text for p_tag in p_tags]
        else:
            paragraphs = re.findall("[А-Я].+[.!?]", parent.text)

        paragraphs = ArticleParser.cleaner.clean_empty_paragraphs(paragraphs)

        return paragraphs

    def _get_raw_paragraphs(self) -> list:
        """ Метод, возвращающий все куски текста, которые соответствуют
        регулярному выражению ArticleParser.article_paragraph_regex

        :return list: список найденных кусков текста
        """

        return self._soup.body.find_all(text=ArticleParser.article_paragraph_regex)

    def _get_raw_parents(self, paragraphs: list) -> list:
        """ Метод, который для каждого найденного куска текста возвращает родителя

        :return list: список родителей
        """

        parents = []
        for paragraph in paragraphs:
            parents.append(paragraph.parent.parent)

        return parents

    def _get_title(self) -> str:
        """ Метод, возвращающий заголовок статьи

        :return str: заголовок статьи
        """

        return self._soup.find("title").text

    def _handle_a(self, bs: BeautifulSoup) -> BeautifulSoup:
        """ Метод, который вставляет в текст URL в квадратных скобках

        :param BeautifulSoupbs: объект BeautifulSoup, в котором нужно изменить ссылки на ссылки в квадратных скобках
        :return BeautifulSoup: объект BeautifulSoup с измененными ссылками
        """

        a_tags = bs.find_all("a")
        replace_with = {}

        for a in a_tags:
            href = a.get("href")
            if not href.startswith("http"):
                href = f"{self._domain}{href}"
            replace_with[str(a)] = f"{a.text} [{href}]"

        bs = str(bs)

        for old, new in replace_with.items():
            bs = bs.replace(old, new, 1)

        return BeautifulSoup(bs, "lxml").html.body.contents[0]

    @property
    def _url_is_available(self) -> bool:
        """ Метод, проверяющий доступность статьи по указанному url

        Если после get-запроса сервер ответил кодом 200, то в атрибуты domain, html и soup записываются
        соответствующие данные.  В противном случае в атрибут http_response записывается результат
        get-запроса response, который в дальнейшем используется для распознавания причины неудачного запроса
        по значению status_code

        :return: True или False
        """

        response = requests.get(self._url)
        if response.status_code == 200:
            self._domain, self._url_tail = self._get_domain_tail()
            self._html = response.text
            self._soup = BeautifulSoup(self._html, "lxml")
            return True

        self._http_response = response

        return False
